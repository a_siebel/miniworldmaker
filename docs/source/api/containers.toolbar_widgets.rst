Toolbar Widgets
***************


ToolbarWidget
---------------

.. autoclass:: miniworldmaker.containers.widgets.Widget
   :members:

   .. autoclasstoc::

ToolbarButton
---------------


.. autoclass:: miniworldmaker.containers.widgets.Button
   :members:

   .. autoclasstoc::

ToolbarLabel
---------------

.. autoclass:: miniworldmaker.containers.widgets.Label
   :members:

   .. autoclasstoc::
