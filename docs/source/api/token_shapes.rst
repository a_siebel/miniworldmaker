Tokens -> Shapes
#############

Shape
======

Base class for shapes


.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Shape
   :members:


Point
=====

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Point
   :members:

Circle
======

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Circle
   :members:

Ellipse
=======

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Ellipse
   :members:

Line
====

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Line
   :members:

Rectangle
=========

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Rectangle
   :members:


Polygon
=======

.. autoclass:: miniworldmaker.tokens.token_plugins.shapes.shapes.Polygon
   :members:
