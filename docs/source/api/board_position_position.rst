Positions -> Position
**********************

.. autoclass:: miniworldmaker.positions.position.Position
   :members:

   .. autoclasstoc::
